question1();
question2();
question3();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};
  
    function objectToHTML(testObj){
        var result="";
        for(var property in testObj){
       result += property +":"+testObj[property] +"\n"
    }
        return result;
    }
    output += objectToHTML(testObj);
    let outPutArea = document.getElementById("outputArea1") 
 outPutArea.innerText = output
}
 
 
 

   //Question 2 here 
function question2(){
var output = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}
   function plus(num1, num2) {
return num1+num2; 
} 
    function multi(num3, num4){
        return num3*num4;
    }
output += flexible(plus, 3, 5) + "\n";
output += flexible(multi, 3, 5) + "\n"; 
let outputArea = document.getElementById("outputArea2");
outputArea.innerHTML = output;
}
    
function question3(){
    let output = "" ;
    var  values =  [4, 3, 6, 12, 1, 3, 8];
    var min= values[0];
    var max= values[0];
    //Question 3 here 
/* Pseudocode
Create a function to find and return minimum and maximum
element in the array.
/**
  a loop through element of array starting from second element (index 1 until last element  in the array.
 */
    /**
      if element at index i is less than current min then set element index i as current minimum.
     */
    /**
      if element at index i is greater than current max then set element at index i as current maximum.
     */
    // return minimum and maximum.
 
    function extremeValue(array) {
 for(let i = 1; i < values.length; i++) {
  if(values[i] < min) {
      min = values[i]; 
    }
 }
 for (let i = 1; i < values.length; i++)
  // return minimum and maximum.
     if( values[i] >max){ max= values[i]
                       }
  return [min,max];
 }

/**question 4 
 * Creating an integer array and printing  output of extremeValue() function.
 */
  
    output +=extremeValue(values)+"\n";

  let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output
}